#
# Copyright (C) 2011 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifeq ($(TARGET_PREBUILT_KERNEL),)
    LOCAL_KERNEL := $(LOCAL_PATH)/prebuilt/kernel
else
    LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

DEVICE_PACKAGE_OVERLAYS := $(LOCAL_PATH)/overlay

# Prebuilt Kernel Image + Modules

PRODUCT_COPY_FILES := \
    $(LOCAL_KERNEL):kernel

PRODUCT_COPY_FILES := \
    $(LOCAL_PATH)/prebuilt/lib/egl/libEGL_perfhud.so:system/lib/egl/libEGL_perfhud.so \
    $(LOCAL_PATH)/prebuilt/lib/egl/libEGL_tegra.so:system/lib/egl/libEGL_tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/egl/libGLESv1_CM_perfhud.so:system/lib/egl/libGLESv1_CM_perfhud.so \
    $(LOCAL_PATH)/prebuilt/lib/egl/libGLESv1_CM_tegra.so:system/lib/egl/libGLESv1_CM_tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/egl/libGLESv2_perfhud.so:system/lib/egl/libGLESv2_perfhud.so \
    $(LOCAL_PATH)/prebuilt/lib/egl/libGLESv2_tegra.so:system/lib/egl/libGLESv2_tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/camera.tegra.so:system/lib/hw/camera.tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/gps.ventana.so:system/lib/hw/gps.ventana.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/gralloc.tegra.so:system/lib/hw/gralloc.tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/hwcomposer.tegra.so:system/lib/hw/hwcomposer.tegra.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/lights.ventana.so:system/lib/hw/lights.ventana.so \
    $(LOCAL_PATH)/prebuilt/lib/hw/sensors.ventana.so:system/lib/hw/sensors.ventana.so \
    $(LOCAL_PATH)/prebuilt/lib/libardrv_dynamic.so:system/lib/libardrv_dynamic.so \
    $(LOCAL_PATH)/prebuilt/lib/libasound.so:system/lib/libasound.so \
    $(LOCAL_PATH)/prebuilt/lib/libcgdrv.so:system/lib/libcgdrv.so \
    $(LOCAL_PATH)/prebuilt/lib/libmllite.so:system/lib/libmllite.so \
    $(LOCAL_PATH)/prebuilt/lib/libmpl.so:system/lib/libmpl.so \
    $(LOCAL_PATH)/prebuilt/lib/libmlplatform.so:system/lib/libmlplatform.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvasfparserhal.so:system/lib/libnvasfparserhal.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvaviparserhal.so:system/lib/libnvaviparserhal.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvavp.so:system/lib/libnvavp.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvcpud_client.so:system/lib/libnvcpud_client.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvcpud.so:system/lib/libnvcpud.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvddk_2d.so:system/lib/libnvddk_2d.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvddk_2d_v2.so:system/lib/libnvddk_2d_v2.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvdispmgr_d.so:system/lib/libnvdispmgr_d.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvhdmi3dplay_jni.so:system/lib/libnvhdmi3dplay_jni.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_asfparser.so:system/lib/libnvmm_asfparser.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_audio.so:system/lib/libnvmm_audio.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_aviparser.so:system/lib/libnvmm_aviparser.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_camera.so:system/lib/libnvmm_camera.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_contentpipe.so:system/lib/libnvmm_contentpipe.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_image.so:system/lib/libnvmm_image.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmmlite_audio.so:system/lib/libnvmmlite_audio.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmmlite.so:system/lib/libnvmmlite.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmmlite_utils.so:system/lib/libnvmmlite_utils.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmmlite_video.so:system/lib/libnvmmlite_video.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_manager.so:system/lib/libnvmm_manager.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_misc.so:system/lib/libnvmm_misc.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_msaudio.so:system/lib/libnvmm_msaudio.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_parser.so:system/lib/libnvmm_parser.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_service.so:system/lib/libnvmm_service.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm.so:system/lib/libnvmm.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_utils.so:system/lib/libnvmm_utils.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_vc1_video.so:system/lib/libnvmm_vc1_video.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_video.so:system/lib/libnvmm_video.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_vp6_video.so:system/lib/libnvmm_vp6_video.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvmm_writer.so:system/lib/libnvmm_writer.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvodm_dtvtuner.so:system/lib/libnvodm_dtvtuner.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvodm_hdmi.so:system/lib/libnvodm_hdmi.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvodm_imager.so:system/lib/libnvodm_imager.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvodm_misc.so:system/lib/libnvodm_misc.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvodm_query.so:system/lib/libnvodm_query.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvomxadaptor.so:system/lib/libnvomxadaptor.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvomxilclient.so:system/lib/libnvomxilclient.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvomx.so:system/lib/libnvomx.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvos.so:system/lib/libnvos.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvparser.so:system/lib/libnvparser.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvrm_graphics.so:system/lib/libnvrm_graphics.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvrm.so:system/lib/libnvrm.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvsm.so:system/lib/libnvsm.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvsystemuiext_jni.so:system/lib/libnvsystemuiext_jni.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvtestio.so:system/lib/libnvtestio.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvtestresults.so:system/lib/libnvtestresults.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvtvmr.so:system/lib/libnvtvmr.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvwinsys.so:system/lib/libnvwinsys.so \
    $(LOCAL_PATH)/prebuilt/lib/libnvwsi.so:system/lib/libnvwsi.so \
    $(LOCAL_PATH)/prebuilt/lib/libstagefrighthw.so:system/lib/libstagefrighthw.so \
    $(LOCAL_PATH)/prebuilt/lib/libsensors.al3000a.so:system/lib/libsensors.al3000a.so \
    $(LOCAL_PATH)/prebuilt/lib/libsensors.isl29018.so:system/lib/libsensors.isl29018.so \
    $(LOCAL_PATH)/prebuilt/lib/libsensors.base.so:system/lib/libsensors.base.so \
    $(LOCAL_PATH)/prebuilt/lib/libsensors.mpl.so:system/lib/libsensors.mpl.so

# Bluetooth configuration files
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/etc/firmware/bcm4329.hcd:system/etc/firmware/bcm4329.hcd \
    $(LOCAL_PATH)/prebuilt/etc/bluetooth/bdaddr:system/etc/bluetooth/bdaddr \
    $(LOCAL_PATH)/prebuilt/etc/bluetooth/bt_vendor.conf:system/etc/bluetooth/bt_vendor.conf

# Ramdisk files
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/fstab.ventana:root/fstab.ventana \
    $(LOCAL_PATH)/ramdisk/init.ventana.rc:root/init.ventana.rc \
    $(LOCAL_PATH)/ramdisk/init.ventana.usb.rc:root/init.ventana.usb.rc \
    $(LOCAL_PATH)/ramdisk/ueventd.ventana.rc:root/ueventd.ventana.rc \
    $(LOCAL_PATH)/ramdisk/init.rc:root/init.rc

# tf101g permissions
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml

# tf101g
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/etc/firmware/modem/EM820W_11.810.09.17.00.zip:system/etc/firmware/modem/EM820W_11.810.09.17.00.zip \
    $(LOCAL_PATH)/prebuilt/etc/ppp/ip-down-HUAWEI:system/etc/ppp/ip-down-HUAWEI \
    $(LOCAL_PATH)/prebuilt/etc/ppp/ip-up-HUAWEI:system/etc/ppp/ip-up-HUAWEI \
    $(LOCAL_PATH)/prebuilt/lib/libhuawei-ril.so:system/lib/libhuawei-ril.so

# Misc
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/bin/sensors-config:system/bin/sensors-config \
    $(LOCAL_PATH)/prebuilt/bin/glgps:system/bin/glgps \
    $(LOCAL_PATH)/prebuilt/bin/wifimacwriter:system/bin/wifimacwriter \
    $(LOCAL_PATH)/prebuilt/xbin/rsync:system/xbin/rsync \
    $(LOCAL_PATH)/prebuilt/data/srs_processing.cfg:system/data/srs_processing.cfg \
    $(LOCAL_PATH)/audio/asound.conf:system/etc/asound.conf \
    $(LOCAL_PATH)/prebuilt/etc/gps/gpsconfig.xml:system/etc/gps/gpsconfig.xml \
    $(LOCAL_PATH)/prebuilt/wifi/nvram_murata.txt:system/etc/nvram_murata.txt \
    $(LOCAL_PATH)/prebuilt/wifi/nvram_nh615_sl101.txt:system/etc/nvram_nh615_sl101.txt \
    $(LOCAL_PATH)/prebuilt/wifi/nvram_nh615.txt:system/etc/nvram_nh615.txt \
    $(LOCAL_PATH)/prebuilt/wifi/nvram.txt:system/etc/nvram.txt \
    $(LOCAL_PATH)/prebuilt/etc/wifi/wpa_supplicant_overlay.conf:system/etc/wifi/wpa_supplicant_overlay.conf \
    $(LOCAL_PATH)/prebuilt/etc/wifi/p2p_supplicant_overlay.conf:system/etc/wifi/p2p_supplicant_overlay.conf

# Input stuff
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/usr/idc/atmel-maxtouch.idc:system/usr/idc/atmel-maxtouch.idc \
    $(LOCAL_PATH)/prebuilt/usr/idc/elantech_touchscreen.idc:system/usr/idc/elantech_touchscreen.idc \
    $(LOCAL_PATH)/prebuilt/usr/idc/panjit_touch.idc:system/usr/idc/panjit_touch.idc \
    $(LOCAL_PATH)/prebuilt/usr/keychars/asusec.kcm:system/usr/keychars/asusec.kcm \
    $(LOCAL_PATH)/prebuilt/usr/keylayout/asusec.kl:system/usr/keylayout/asusec.kl \
    $(LOCAL_PATH)/prebuilt/usr/keylayout/gpio-keys.kl:system/usr/keylayout/gpio-keys.kl

# Vendor firmware
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_aacdec.axf:system/etc/firmware/nvmm_aacdec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_adtsdec.axf:system/etc/firmware/nvmm_adtsdec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_h264dec.axf:system/etc/firmware/nvmm_h264dec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_jpegdec.axf:system/etc/firmware/nvmm_jpegdec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_jpegenc.axf:system/etc/firmware/nvmm_jpegenc.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_manager.axf:system/etc/firmware/nvmm_manager.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_mp3dec.axf:system/etc/firmware/nvmm_mp3dec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_mpeg4dec.axf:system/etc/firmware/nvmm_mpeg4dec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_reference.axf:system/etc/firmware/nvmm_reference.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_service.axf:system/etc/firmware/nvmm_service.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_sorensondec.axf:system/etc/firmware/nvmm_sorensondec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_vc1dec.axf:system/etc/firmware/nvmm_vc1dec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_wavdec.axf:system/etc/firmware/nvmm_wavdec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_wmadec.axf:system/etc/firmware/nvmm_wmadec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvmm_wmaprodec.axf:system/etc/firmware/nvmm_wmaprodec.axf \
    $(LOCAL_PATH)/prebuilt/etc/firmware/nvrm_avp.bin:system/etc/firmware/nvrm_avp.bin \
    $(LOCAL_PATH)/prebuilt/vendor/firmware/fw_bcmdhd.bin:system/vendor/firmware/fw_bcmdhd.bin \
    $(LOCAL_PATH)/prebuilt/vendor/firmware/fw_bcmdhd_apsta.bin:system/vendor/firmware/fw_bcmdhd_apsta.bin \
    $(LOCAL_PATH)/prebuilt/vendor/firmware/fw_bcmdhd_p2p.bin:system/vendor/firmware/fw_bcmdhd_p2p.bin

# ALSA Config files
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/alsa.conf:system/usr/share/alsa/alsa.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/cards/aliases.conf:system/usr/share/alsa/cards/aliases.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/center_lfe.conf:system/usr/share/alsa/pcm/center_lfe.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/default.conf:system/usr/share/alsa/pcm/default.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/dmix.conf:system/usr/share/alsa/pcm/dmix.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/dpl.conf:system/usr/share/alsa/pcm/dpl.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/dsnoop.conf:system/usr/share/alsa/pcm/dsnoop.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/front.conf:system/usr/share/alsa/pcm/front.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/iec958.conf:system/usr/share/alsa/pcm/iec958.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/modem.conf:system/usr/share/alsa/pcm/modem.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/rear.conf:system/usr/share/alsa/pcm/rear.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/side.conf:system/usr/share/alsa/pcm/side.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/surround40.conf:system/usr/share/alsa/pcm/surround40.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/surround41.conf:system/usr/share/alsa/pcm/surround41.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/surround50.conf:system/usr/share/alsa/pcm/surround50.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/surround51.conf:system/usr/share/alsa/pcm/surround51.conf \
    $(LOCAL_PATH)/prebuilt/usr/share/alsa/pcm/surround71.conf:system/usr/share/alsa/pcm/surround71.conf

# kernel modules
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/lib/modules/apm-emulation.ko:system/lib/modules/apm-emulation.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/battery_rvsd.ko:system/lib/modules/battery_rvsd.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/ipip.ko:system/lib/modules/ipip.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/isofs.ko:system/lib/modules/isofs.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/ntfs.ko:system/lib/modules/ntfs.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/scsi_wait_scan.ko:system/lib/modules/scsi_wait_scan.ko \
    $(LOCAL_PATH)/prebuilt/lib/modules/udf.ko:system/lib/modules/udf.ko

# init.d
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/etc/init.d/05modules:system/etc/init.d/05modules

PRODUCT_PROPERTY_OVERRIDES := \
    wifi.interface=wlan0 \
    wifi.supplicant_scan_interval=15 \
    ro.sf.lcd_density=160

# Set default USB interface
PRODUCT_DEFAULT_PROPERTY_OVERRIDES += \
    persist.sys.usb.config=mtp

include frameworks/native/build/tablet-dalvik-heap.mk

# These are the hardware-specific features
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/tablet_core_hardware.xml:system/etc/permissions/tablet_core_hardware.xml \
    frameworks/native/data/etc/android.hardware.camera.autofocus.xml:system/etc/permissions/android.hardware.camera.autofocus.xml \
    frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
    frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
    frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml \
    frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
    frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
    frameworks/native/data/etc/android.hardware.sensor.gyroscope.xml:system/etc/permissions/android.hardware.sensor.gyroscope.xml \
    frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml \
    frameworks/native/data/etc/android.hardware.usb.accessory.xml:system/etc/permissions/android.hardware.usb.accessory.xml \
    frameworks/native/data/etc/android.hardware.usb.host.xml:system/etc/permissions/android.hardware.usb.host.xml \
    $(LOCAL_PATH)/prebuilt/etc/permissions/android.hardware.tf101g.xml:system/etc/permissions/android.hardware.tf101g.xml

PRODUCT_PACKAGES := \
    audio_policy.tegra \
    audio.primary.tegra \
    audio.a2dp.default \
    audio.usb.default \
    librs_jni \
    l2ping \
    hcitool \
    bttest \
    com.android.future.usb.accessory \
    whisperd \
    libaudioutils \
    libinvensense_mpl \
    blobpack_tf

# Phone packages
PRODUCT_PACKAGES += \
    Stk \
    Phone \
    Contacts \
    Mms

# ft101g override
PRODUCT_PROPERTY_OVERRIDES += \
    ro.pad.features.modem=true \
    gsm.bodysargsm=32,26,29,29 \
    ro.com.android.dataroaming=false \
    ro.cgsms.mode=1 \
    persist.radio.apm_sim_not_pwdn=true

PRODUCT_PROPERTY_OVERRIDES += \
    ro.setupwizard.enterprise_mode=1

# My override
PRODUCT_PROPERTY_OVERRIDES += \
    ro.product.locale.language=ru \
    ro.product.locale.region=RU \
    ro.com.android.dateformat=dd-MM-yyyy

# adb has root
ADDITIONAL_DEFAULT_PROPERTIES += \
    ro.secure=0 \
    ro.allow.mock.location=1 \
    ro.adb.secure=0 \
    ro.debuggable=1

PRODUCT_CHARACTERISTICS := tablet

# we have enough storage space to hold precise GC data
PRODUCT_TAGS += dalvik.gc.type-precise

# media config xml file
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/media/media_profiles.xml:system/etc/media_profiles.xml

# media codec config xml file
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/media/media_codecs.xml:system/etc/media_codecs.xml

# Camera config file
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/prebuilt/etc/nvcamera.conf:system/etc/nvcamera.conf

# Bluetooth config file
#PRODUCT_COPY_FILES += \
#    system/bluetooth/data/main.nonsmartphone.conf:system/etc/bluetooth/main.conf

# audio mixer paths
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/audio/mixer_paths.xml:system/etc/mixer_paths.xml

# audio policy configuration
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/audio/audio_policy.conf:system/etc/audio_policy.conf

$(call inherit-product-if-exists, vendor/asus/tf101g/device-vendor.mk)

ifneq ($(EOS_RELEASE),)
PRODUCT_PROPERTY_OVERRIDES += \
    ro.rommanager.developerid=teameos \
    ro.modversion=Eos-$(EOS_RELEASE)-TF101G
endif
